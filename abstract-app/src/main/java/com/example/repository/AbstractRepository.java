package com.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.AbstractInformationEntity;

public interface AbstractRepository extends CrudRepository<AbstractInformationEntity, Integer> {

}
