package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.AbstractInformationEntity;
import com.example.model.UpdateAbstractData;
import com.example.service.AbstractService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("demo")
public class AbstractController {

	@Autowired
	protected AbstractService appService;

	/**
	 * Return all the abstracts.
	 * @return
	 */
	@ApiOperation(value = "Get all Abstracts.")
	@GetMapping("/return-abstracts")
	public @ResponseBody Iterable<AbstractInformationEntity> returnAbstracts() {
		
		return appService.getAllAbstracts();
	}
	
	/**
	 * Return abstract based on abstract-id.
	 * @return
	 */
	@ApiOperation(value = "Get abstract based on abstract-id.")
	@GetMapping("/return-abstract/{abstractId}")
	public @ResponseBody AbstractInformationEntity returnSingleAbstract(@PathVariable(value="abstractId") int abstractId) {
		
		return appService.getSingleAbstract(abstractId);
	}
	
	/**
	 * Remove an abstract.
	 * @return
	 */
	@ApiOperation(value = "Delete an abstract based on an abstract-id.")
	@DeleteMapping("/remove-abstract/{abstractId}")
	public @ResponseBody String removeAbstract(@PathVariable(value="abstractId") int abstractId) {
		
		return appService.deleteAbstract(abstractId);
	}
	
	/**
	 * Update Abstract information and abstract indicator.
	 * @param abstractId
	 * @param abstractData
	 * @return
	 */
	@ApiOperation(value = "Update an abstract based on an abstract-id")
	@PutMapping("/process-abstract/{abstractId}")
	public @ResponseBody String updateAbstract(@PathVariable(value="abstractId") int abstractId,
			@RequestBody UpdateAbstractData abstractData) {
		
		String successMsg = appService.updateAbstractData(abstractId, abstractData);
		return successMsg;
		
	}
	
	@ApiOperation(value = "Create a new abstract.")
	@PostMapping("/add-abstract")
	public @ResponseBody AbstractInformationEntity addAbstract(@RequestBody AbstractInformationEntity abstractData) {
		return appService.createAbstract(abstractData);
	}
	
}
