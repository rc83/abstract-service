package com.example.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="budget_info")
public class BudgetInfomationEntity {
	
	public BudgetInfomationEntity() {
		
	}
	
	@Id
	private Integer budgetPerdId;
	@NotNull
	private Integer applicationId;
	private Integer budgetPerdSeq;
	private Integer budgetPerdFy;
	private Date budgetPerdStartDate;
	private Date budgetPerdEndDate;
	@NotNull
	private String budgetStatusCd;
	private String budgetPerdCompleteInd;
	private String fapiisCertificateInd;
	private Integer participantDecimal;
	private String samCertificateInd;
	private Float personnelReqFedAmt;
	private Float personnelNonFedAmt;
	private Float fringeReqFedAmt;
	private Float fringeRecFedAmt;
	private Float fringeNonFedAmt;
	private Float travelReqFedAmt;
	private Float travelRecFedAmt;
	private Float travelNonFedAmt;
	private Float equipReqFedAmt;
	private Float equipRecFedAmt;
	private Float equipNonFedAmt;
	private Float supplyReqFedAmt;
	private Float supplyRecFedAmt;
	private Float supplyNonFedAmt;
	private Float contractReqFedAmt;
	private Float contractRecFedAmt;
	private Float contractNonFedAmt;
	private Float constructReqFedAmt;
	private Float constructRecFedAmt;
	private Float constructNonFedAmt;
	private Float otherReqFedAmt;
	private Float otherRecFedAmt;
	private Float otherNonFedAmt;
	private Float trainingReqFedAmt;
	private Float trainingRecFedAmt;
	private Float trainingNonFedAmt;
	private Float indirectReqFedAmt;
	private Float indirectRecFedAmt;
	private Float indirectNonFedAmt;
	public Integer getBudgetPerdId() {
		return budgetPerdId;
	}
	public void setBudgetPerdId(Integer budgetPerdId) {
		this.budgetPerdId = budgetPerdId;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getBudgetPerdSeq() {
		return budgetPerdSeq;
	}
	public void setBudgetPerdSeq(Integer budgetPerdSeq) {
		this.budgetPerdSeq = budgetPerdSeq;
	}
	public Integer getBudgetPerdFy() {
		return budgetPerdFy;
	}
	public void setBudgetPerdFy(Integer budgetPerdFy) {
		this.budgetPerdFy = budgetPerdFy;
	}
	public Date getBudgetPerdStartDate() {
		return budgetPerdStartDate;
	}
	public void setBudgetPerdStartDate(Date budgetPerdStartDate) {
		this.budgetPerdStartDate = budgetPerdStartDate;
	}
	public Date getBudgetPerdEndDate() {
		return budgetPerdEndDate;
	}
	public void setBudgetPerdEndDate(Date budgetPerdEndDate) {
		this.budgetPerdEndDate = budgetPerdEndDate;
	}
	public String getBudgetStatusCd() {
		return budgetStatusCd;
	}
	public void setBudgetStatusCd(String budgetStatusCd) {
		this.budgetStatusCd = budgetStatusCd;
	}
	public String getBudgetPerdCompleteInd() {
		return budgetPerdCompleteInd;
	}
	public void setBudgetPerdCompleteInd(String budgetPerdCompleteInd) {
		this.budgetPerdCompleteInd = budgetPerdCompleteInd;
	}
	public String getFapiisCertificateInd() {
		return fapiisCertificateInd;
	}
	public void setFapiisCertificateInd(String fapiisCertificateInd) {
		this.fapiisCertificateInd = fapiisCertificateInd;
	}
	public Integer getParticipantDecimal() {
		return participantDecimal;
	}
	public void setParticipantDecimal(Integer participantDecimal) {
		this.participantDecimal = participantDecimal;
	}
	public String getSamCertificateInd() {
		return samCertificateInd;
	}
	public void setSamCertificateInd(String samCertificateInd) {
		this.samCertificateInd = samCertificateInd;
	}
	public Float getPersonnelReqFedAmt() {
		return personnelReqFedAmt;
	}
	public void setPersonnelReqFedAmt(Float personnelReqFedAmt) {
		this.personnelReqFedAmt = personnelReqFedAmt;
	}
	public Float getPersonnelNonFedAmt() {
		return personnelNonFedAmt;
	}
	public void setPersonnelNonFedAmt(Float personnelNonFedAmt) {
		this.personnelNonFedAmt = personnelNonFedAmt;
	}
	public Float getFringeReqFedAmt() {
		return fringeReqFedAmt;
	}
	public void setFringeReqFedAmt(Float fringeReqFedAmt) {
		this.fringeReqFedAmt = fringeReqFedAmt;
	}
	public Float getFringeRecFedAmt() {
		return fringeRecFedAmt;
	}
	public void setFringeRecFedAmt(Float fringeRecFedAmt) {
		this.fringeRecFedAmt = fringeRecFedAmt;
	}
	public Float getFringeNonFedAmt() {
		return fringeNonFedAmt;
	}
	public void setFringeNonFedAmt(Float fringeNonFedAmt) {
		this.fringeNonFedAmt = fringeNonFedAmt;
	}
	public Float getTravelReqFedAmt() {
		return travelReqFedAmt;
	}
	public void setTravelReqFedAmt(Float travelReqFedAmt) {
		this.travelReqFedAmt = travelReqFedAmt;
	}
	public Float getTravelRecFedAmt() {
		return travelRecFedAmt;
	}
	public void setTravelRecFedAmt(Float travelRecFedAmt) {
		this.travelRecFedAmt = travelRecFedAmt;
	}
	public Float getTravelNonFedAmt() {
		return travelNonFedAmt;
	}
	public void setTravelNonFedAmt(Float travelNonFedAmt) {
		this.travelNonFedAmt = travelNonFedAmt;
	}
	public Float getEquipReqFedAmt() {
		return equipReqFedAmt;
	}
	public void setEquipReqFedAmt(Float equipReqFedAmt) {
		this.equipReqFedAmt = equipReqFedAmt;
	}
	public Float getEquipRecFedAmt() {
		return equipRecFedAmt;
	}
	public void setEquipRecFedAmt(Float equipRecFedAmt) {
		this.equipRecFedAmt = equipRecFedAmt;
	}
	public Float getEquipNonFedAmt() {
		return equipNonFedAmt;
	}
	public void setEquipNonFedAmt(Float equipNonFedAmt) {
		this.equipNonFedAmt = equipNonFedAmt;
	}
	public Float getSupplyReqFedAmt() {
		return supplyReqFedAmt;
	}
	public void setSupplyReqFedAmt(Float supplyReqFedAmt) {
		this.supplyReqFedAmt = supplyReqFedAmt;
	}
	public Float getSupplyRecFedAmt() {
		return supplyRecFedAmt;
	}
	public void setSupplyRecFedAmt(Float supplyRecFedAmt) {
		this.supplyRecFedAmt = supplyRecFedAmt;
	}
	public Float getSupplyNonFedAmt() {
		return supplyNonFedAmt;
	}
	public void setSupplyNonFedAmt(Float supplyNonFedAmt) {
		this.supplyNonFedAmt = supplyNonFedAmt;
	}
	public Float getContractReqFedAmt() {
		return contractReqFedAmt;
	}
	public void setContractReqFedAmt(Float contractReqFedAmt) {
		this.contractReqFedAmt = contractReqFedAmt;
	}
	public Float getContractRecFedAmt() {
		return contractRecFedAmt;
	}
	public void setContractRecFedAmt(Float contractRecFedAmt) {
		this.contractRecFedAmt = contractRecFedAmt;
	}
	public Float getContractNonFedAmt() {
		return contractNonFedAmt;
	}
	public void setContractNonFedAmt(Float contractNonFedAmt) {
		this.contractNonFedAmt = contractNonFedAmt;
	}
	public Float getConstructReqFedAmt() {
		return constructReqFedAmt;
	}
	public void setConstructReqFedAmt(Float constructReqFedAmt) {
		this.constructReqFedAmt = constructReqFedAmt;
	}
	public Float getConstructRecFedAmt() {
		return constructRecFedAmt;
	}
	public void setConstructRecFedAmt(Float constructRecFedAmt) {
		this.constructRecFedAmt = constructRecFedAmt;
	}
	public Float getConstructNonFedAmt() {
		return constructNonFedAmt;
	}
	public void setConstructNonFedAmt(Float constructNonFedAmt) {
		this.constructNonFedAmt = constructNonFedAmt;
	}
	public Float getOtherReqFedAmt() {
		return otherReqFedAmt;
	}
	public void setOtherReqFedAmt(Float otherReqFedAmt) {
		this.otherReqFedAmt = otherReqFedAmt;
	}
	public Float getOtherRecFedAmt() {
		return otherRecFedAmt;
	}
	public void setOtherRecFedAmt(Float otherRecFedAmt) {
		this.otherRecFedAmt = otherRecFedAmt;
	}
	public Float getOtherNonFedAmt() {
		return otherNonFedAmt;
	}
	public void setOtherNonFedAmt(Float otherNonFedAmt) {
		this.otherNonFedAmt = otherNonFedAmt;
	}
	public Float getTrainingReqFedAmt() {
		return trainingReqFedAmt;
	}
	public void setTrainingReqFedAmt(Float trainingReqFedAmt) {
		this.trainingReqFedAmt = trainingReqFedAmt;
	}
	public Float getTrainingRecFedAmt() {
		return trainingRecFedAmt;
	}
	public void setTrainingRecFedAmt(Float trainingRecFedAmt) {
		this.trainingRecFedAmt = trainingRecFedAmt;
	}
	public Float getTrainingNonFedAmt() {
		return trainingNonFedAmt;
	}
	public void setTrainingNonFedAmt(Float trainingNonFedAmt) {
		this.trainingNonFedAmt = trainingNonFedAmt;
	}
	public Float getIndirectReqFedAmt() {
		return indirectReqFedAmt;
	}
	public void setIndirectReqFedAmt(Float indirectReqFedAmt) {
		this.indirectReqFedAmt = indirectReqFedAmt;
	}
	public Float getIndirectRecFedAmt() {
		return indirectRecFedAmt;
	}
	public void setIndirectRecFedAmt(Float indirectRecFedAmt) {
		this.indirectRecFedAmt = indirectRecFedAmt;
	}
	public Float getIndirectNonFedAmt() {
		return indirectNonFedAmt;
	}
	public void setIndirectNonFedAmt(Float indirectNonFedAmt) {
		this.indirectNonFedAmt = indirectNonFedAmt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicationId == null) ? 0 : applicationId.hashCode());
		result = prime * result + ((budgetPerdCompleteInd == null) ? 0 : budgetPerdCompleteInd.hashCode());
		result = prime * result + ((budgetPerdEndDate == null) ? 0 : budgetPerdEndDate.hashCode());
		result = prime * result + ((budgetPerdFy == null) ? 0 : budgetPerdFy.hashCode());
		result = prime * result + ((budgetPerdId == null) ? 0 : budgetPerdId.hashCode());
		result = prime * result + ((budgetPerdSeq == null) ? 0 : budgetPerdSeq.hashCode());
		result = prime * result + ((budgetPerdStartDate == null) ? 0 : budgetPerdStartDate.hashCode());
		result = prime * result + ((budgetStatusCd == null) ? 0 : budgetStatusCd.hashCode());
		result = prime * result + ((constructNonFedAmt == null) ? 0 : constructNonFedAmt.hashCode());
		result = prime * result + ((constructRecFedAmt == null) ? 0 : constructRecFedAmt.hashCode());
		result = prime * result + ((constructReqFedAmt == null) ? 0 : constructReqFedAmt.hashCode());
		result = prime * result + ((contractNonFedAmt == null) ? 0 : contractNonFedAmt.hashCode());
		result = prime * result + ((contractRecFedAmt == null) ? 0 : contractRecFedAmt.hashCode());
		result = prime * result + ((contractReqFedAmt == null) ? 0 : contractReqFedAmt.hashCode());
		result = prime * result + ((equipNonFedAmt == null) ? 0 : equipNonFedAmt.hashCode());
		result = prime * result + ((equipRecFedAmt == null) ? 0 : equipRecFedAmt.hashCode());
		result = prime * result + ((equipReqFedAmt == null) ? 0 : equipReqFedAmt.hashCode());
		result = prime * result + ((fapiisCertificateInd == null) ? 0 : fapiisCertificateInd.hashCode());
		result = prime * result + ((fringeNonFedAmt == null) ? 0 : fringeNonFedAmt.hashCode());
		result = prime * result + ((fringeRecFedAmt == null) ? 0 : fringeRecFedAmt.hashCode());
		result = prime * result + ((fringeReqFedAmt == null) ? 0 : fringeReqFedAmt.hashCode());
		result = prime * result + ((indirectNonFedAmt == null) ? 0 : indirectNonFedAmt.hashCode());
		result = prime * result + ((indirectRecFedAmt == null) ? 0 : indirectRecFedAmt.hashCode());
		result = prime * result + ((indirectReqFedAmt == null) ? 0 : indirectReqFedAmt.hashCode());
		result = prime * result + ((otherNonFedAmt == null) ? 0 : otherNonFedAmt.hashCode());
		result = prime * result + ((otherRecFedAmt == null) ? 0 : otherRecFedAmt.hashCode());
		result = prime * result + ((otherReqFedAmt == null) ? 0 : otherReqFedAmt.hashCode());
		result = prime * result + ((participantDecimal == null) ? 0 : participantDecimal.hashCode());
		result = prime * result + ((personnelNonFedAmt == null) ? 0 : personnelNonFedAmt.hashCode());
		result = prime * result + ((personnelReqFedAmt == null) ? 0 : personnelReqFedAmt.hashCode());
		result = prime * result + ((samCertificateInd == null) ? 0 : samCertificateInd.hashCode());
		result = prime * result + ((supplyNonFedAmt == null) ? 0 : supplyNonFedAmt.hashCode());
		result = prime * result + ((supplyRecFedAmt == null) ? 0 : supplyRecFedAmt.hashCode());
		result = prime * result + ((supplyReqFedAmt == null) ? 0 : supplyReqFedAmt.hashCode());
		result = prime * result + ((trainingNonFedAmt == null) ? 0 : trainingNonFedAmt.hashCode());
		result = prime * result + ((trainingRecFedAmt == null) ? 0 : trainingRecFedAmt.hashCode());
		result = prime * result + ((trainingReqFedAmt == null) ? 0 : trainingReqFedAmt.hashCode());
		result = prime * result + ((travelNonFedAmt == null) ? 0 : travelNonFedAmt.hashCode());
		result = prime * result + ((travelRecFedAmt == null) ? 0 : travelRecFedAmt.hashCode());
		result = prime * result + ((travelReqFedAmt == null) ? 0 : travelReqFedAmt.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BudgetInfomationEntity other = (BudgetInfomationEntity) obj;
		if (applicationId == null) {
			if (other.applicationId != null)
				return false;
		} else if (!applicationId.equals(other.applicationId))
			return false;
		if (budgetPerdCompleteInd == null) {
			if (other.budgetPerdCompleteInd != null)
				return false;
		} else if (!budgetPerdCompleteInd.equals(other.budgetPerdCompleteInd))
			return false;
		if (budgetPerdEndDate == null) {
			if (other.budgetPerdEndDate != null)
				return false;
		} else if (!budgetPerdEndDate.equals(other.budgetPerdEndDate))
			return false;
		if (budgetPerdFy == null) {
			if (other.budgetPerdFy != null)
				return false;
		} else if (!budgetPerdFy.equals(other.budgetPerdFy))
			return false;
		if (budgetPerdId == null) {
			if (other.budgetPerdId != null)
				return false;
		} else if (!budgetPerdId.equals(other.budgetPerdId))
			return false;
		if (budgetPerdSeq == null) {
			if (other.budgetPerdSeq != null)
				return false;
		} else if (!budgetPerdSeq.equals(other.budgetPerdSeq))
			return false;
		if (budgetPerdStartDate == null) {
			if (other.budgetPerdStartDate != null)
				return false;
		} else if (!budgetPerdStartDate.equals(other.budgetPerdStartDate))
			return false;
		if (budgetStatusCd == null) {
			if (other.budgetStatusCd != null)
				return false;
		} else if (!budgetStatusCd.equals(other.budgetStatusCd))
			return false;
		if (constructNonFedAmt == null) {
			if (other.constructNonFedAmt != null)
				return false;
		} else if (!constructNonFedAmt.equals(other.constructNonFedAmt))
			return false;
		if (constructRecFedAmt == null) {
			if (other.constructRecFedAmt != null)
				return false;
		} else if (!constructRecFedAmt.equals(other.constructRecFedAmt))
			return false;
		if (constructReqFedAmt == null) {
			if (other.constructReqFedAmt != null)
				return false;
		} else if (!constructReqFedAmt.equals(other.constructReqFedAmt))
			return false;
		if (contractNonFedAmt == null) {
			if (other.contractNonFedAmt != null)
				return false;
		} else if (!contractNonFedAmt.equals(other.contractNonFedAmt))
			return false;
		if (contractRecFedAmt == null) {
			if (other.contractRecFedAmt != null)
				return false;
		} else if (!contractRecFedAmt.equals(other.contractRecFedAmt))
			return false;
		if (contractReqFedAmt == null) {
			if (other.contractReqFedAmt != null)
				return false;
		} else if (!contractReqFedAmt.equals(other.contractReqFedAmt))
			return false;
		if (equipNonFedAmt == null) {
			if (other.equipNonFedAmt != null)
				return false;
		} else if (!equipNonFedAmt.equals(other.equipNonFedAmt))
			return false;
		if (equipRecFedAmt == null) {
			if (other.equipRecFedAmt != null)
				return false;
		} else if (!equipRecFedAmt.equals(other.equipRecFedAmt))
			return false;
		if (equipReqFedAmt == null) {
			if (other.equipReqFedAmt != null)
				return false;
		} else if (!equipReqFedAmt.equals(other.equipReqFedAmt))
			return false;
		if (fapiisCertificateInd == null) {
			if (other.fapiisCertificateInd != null)
				return false;
		} else if (!fapiisCertificateInd.equals(other.fapiisCertificateInd))
			return false;
		if (fringeNonFedAmt == null) {
			if (other.fringeNonFedAmt != null)
				return false;
		} else if (!fringeNonFedAmt.equals(other.fringeNonFedAmt))
			return false;
		if (fringeRecFedAmt == null) {
			if (other.fringeRecFedAmt != null)
				return false;
		} else if (!fringeRecFedAmt.equals(other.fringeRecFedAmt))
			return false;
		if (fringeReqFedAmt == null) {
			if (other.fringeReqFedAmt != null)
				return false;
		} else if (!fringeReqFedAmt.equals(other.fringeReqFedAmt))
			return false;
		if (indirectNonFedAmt == null) {
			if (other.indirectNonFedAmt != null)
				return false;
		} else if (!indirectNonFedAmt.equals(other.indirectNonFedAmt))
			return false;
		if (indirectRecFedAmt == null) {
			if (other.indirectRecFedAmt != null)
				return false;
		} else if (!indirectRecFedAmt.equals(other.indirectRecFedAmt))
			return false;
		if (indirectReqFedAmt == null) {
			if (other.indirectReqFedAmt != null)
				return false;
		} else if (!indirectReqFedAmt.equals(other.indirectReqFedAmt))
			return false;
		if (otherNonFedAmt == null) {
			if (other.otherNonFedAmt != null)
				return false;
		} else if (!otherNonFedAmt.equals(other.otherNonFedAmt))
			return false;
		if (otherRecFedAmt == null) {
			if (other.otherRecFedAmt != null)
				return false;
		} else if (!otherRecFedAmt.equals(other.otherRecFedAmt))
			return false;
		if (otherReqFedAmt == null) {
			if (other.otherReqFedAmt != null)
				return false;
		} else if (!otherReqFedAmt.equals(other.otherReqFedAmt))
			return false;
		if (participantDecimal == null) {
			if (other.participantDecimal != null)
				return false;
		} else if (!participantDecimal.equals(other.participantDecimal))
			return false;
		if (personnelNonFedAmt == null) {
			if (other.personnelNonFedAmt != null)
				return false;
		} else if (!personnelNonFedAmt.equals(other.personnelNonFedAmt))
			return false;
		if (personnelReqFedAmt == null) {
			if (other.personnelReqFedAmt != null)
				return false;
		} else if (!personnelReqFedAmt.equals(other.personnelReqFedAmt))
			return false;
		if (samCertificateInd == null) {
			if (other.samCertificateInd != null)
				return false;
		} else if (!samCertificateInd.equals(other.samCertificateInd))
			return false;
		if (supplyNonFedAmt == null) {
			if (other.supplyNonFedAmt != null)
				return false;
		} else if (!supplyNonFedAmt.equals(other.supplyNonFedAmt))
			return false;
		if (supplyRecFedAmt == null) {
			if (other.supplyRecFedAmt != null)
				return false;
		} else if (!supplyRecFedAmt.equals(other.supplyRecFedAmt))
			return false;
		if (supplyReqFedAmt == null) {
			if (other.supplyReqFedAmt != null)
				return false;
		} else if (!supplyReqFedAmt.equals(other.supplyReqFedAmt))
			return false;
		if (trainingNonFedAmt == null) {
			if (other.trainingNonFedAmt != null)
				return false;
		} else if (!trainingNonFedAmt.equals(other.trainingNonFedAmt))
			return false;
		if (trainingRecFedAmt == null) {
			if (other.trainingRecFedAmt != null)
				return false;
		} else if (!trainingRecFedAmt.equals(other.trainingRecFedAmt))
			return false;
		if (trainingReqFedAmt == null) {
			if (other.trainingReqFedAmt != null)
				return false;
		} else if (!trainingReqFedAmt.equals(other.trainingReqFedAmt))
			return false;
		if (travelNonFedAmt == null) {
			if (other.travelNonFedAmt != null)
				return false;
		} else if (!travelNonFedAmt.equals(other.travelNonFedAmt))
			return false;
		if (travelRecFedAmt == null) {
			if (other.travelRecFedAmt != null)
				return false;
		} else if (!travelRecFedAmt.equals(other.travelRecFedAmt))
			return false;
		if (travelReqFedAmt == null) {
			if (other.travelReqFedAmt != null)
				return false;
		} else if (!travelReqFedAmt.equals(other.travelReqFedAmt))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "BudgetInfomationEntity [budgetPerdId=" + budgetPerdId + ", applicationId=" + applicationId
				+ ", budgetPerdSeq=" + budgetPerdSeq + ", budgetPerdFy=" + budgetPerdFy + ", budgetPerdStartDate="
				+ budgetPerdStartDate + ", budgetPerdEndDate=" + budgetPerdEndDate + ", budgetStatusCd="
				+ budgetStatusCd + ", budgetPerdCompleteInd=" + budgetPerdCompleteInd + ", fapiisCertificateInd="
				+ fapiisCertificateInd + ", participantDecimal=" + participantDecimal + ", samCertificateInd="
				+ samCertificateInd + ", personnelReqFedAmt=" + personnelReqFedAmt + ", personnelNonFedAmt="
				+ personnelNonFedAmt + ", fringeReqFedAmt=" + fringeReqFedAmt + ", fringeRecFedAmt=" + fringeRecFedAmt
				+ ", fringeNonFedAmt=" + fringeNonFedAmt + ", travelReqFedAmt=" + travelReqFedAmt + ", travelRecFedAmt="
				+ travelRecFedAmt + ", travelNonFedAmt=" + travelNonFedAmt + ", equipReqFedAmt=" + equipReqFedAmt
				+ ", equipRecFedAmt=" + equipRecFedAmt + ", equipNonFedAmt=" + equipNonFedAmt + ", supplyReqFedAmt="
				+ supplyReqFedAmt + ", supplyRecFedAmt=" + supplyRecFedAmt + ", supplyNonFedAmt=" + supplyNonFedAmt
				+ ", contractReqFedAmt=" + contractReqFedAmt + ", contractRecFedAmt=" + contractRecFedAmt
				+ ", contractNonFedAmt=" + contractNonFedAmt + ", constructReqFedAmt=" + constructReqFedAmt
				+ ", constructRecFedAmt=" + constructRecFedAmt + ", constructNonFedAmt=" + constructNonFedAmt
				+ ", otherReqFedAmt=" + otherReqFedAmt + ", otherRecFedAmt=" + otherRecFedAmt + ", otherNonFedAmt="
				+ otherNonFedAmt + ", trainingReqFedAmt=" + trainingReqFedAmt + ", trainingRecFedAmt="
				+ trainingRecFedAmt + ", trainingNonFedAmt=" + trainingNonFedAmt + ", indirectReqFedAmt="
				+ indirectReqFedAmt + ", indirectRecFedAmt=" + indirectRecFedAmt + ", indirectNonFedAmt="
				+ indirectNonFedAmt + "]";
	}
	
	
}
