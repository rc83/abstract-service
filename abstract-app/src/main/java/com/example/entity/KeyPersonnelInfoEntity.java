package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="key_personnel_info")
public class KeyPersonnelInfoEntity {

	public KeyPersonnelInfoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Id
	private Integer keyPersonnelId;
	private Integer budgetPerdId;
	@NotNull
	private String keyPersonnelTitleCd;
	private String firstName;
	private String lastName;
	private String middleInitial;
	private Float levelOfEffort;
	private String phoneNumber;
	private String email;
	
	public Integer getKeyPersonnelId() {
		return keyPersonnelId;
	}
	public void setKeyPersonnelId(Integer keyPersonnelId) {
		this.keyPersonnelId = keyPersonnelId;
	}
	public Integer getBudgetPerdId() {
		return budgetPerdId;
	}
	public void setBudgetPerdId(Integer budgetPerdId) {
		this.budgetPerdId = budgetPerdId;
	}
	public String getKeyPersonnelTitleCd() {
		return keyPersonnelTitleCd;
	}
	public void setKeyPersonnelTitleCd(String keyPersonnelTitleCd) {
		this.keyPersonnelTitleCd = keyPersonnelTitleCd;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public Float getLevelOfEffort() {
		return levelOfEffort;
	}
	public void setLevelOfEffort(Float levelOfEffort) {
		this.levelOfEffort = levelOfEffort;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((budgetPerdId == null) ? 0 : budgetPerdId.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((keyPersonnelId == null) ? 0 : keyPersonnelId.hashCode());
		result = prime * result + ((keyPersonnelTitleCd == null) ? 0 : keyPersonnelTitleCd.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((levelOfEffort == null) ? 0 : levelOfEffort.hashCode());
		result = prime * result + ((middleInitial == null) ? 0 : middleInitial.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyPersonnelInfoEntity other = (KeyPersonnelInfoEntity) obj;
		if (budgetPerdId == null) {
			if (other.budgetPerdId != null)
				return false;
		} else if (!budgetPerdId.equals(other.budgetPerdId))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (keyPersonnelId == null) {
			if (other.keyPersonnelId != null)
				return false;
		} else if (!keyPersonnelId.equals(other.keyPersonnelId))
			return false;
		if (keyPersonnelTitleCd == null) {
			if (other.keyPersonnelTitleCd != null)
				return false;
		} else if (!keyPersonnelTitleCd.equals(other.keyPersonnelTitleCd))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (levelOfEffort == null) {
			if (other.levelOfEffort != null)
				return false;
		} else if (!levelOfEffort.equals(other.levelOfEffort))
			return false;
		if (middleInitial == null) {
			if (other.middleInitial != null)
				return false;
		} else if (!middleInitial.equals(other.middleInitial))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "KeyPersonnelInfo [keyPersonnelId=" + keyPersonnelId + ", budgetPerdId=" + budgetPerdId
				+ ", keyPersonnelTitleCd=" + keyPersonnelTitleCd + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleInitial=" + middleInitial + ", levelOfEffort=" + levelOfEffort + ", phoneNumber="
				+ phoneNumber + ", email=" + email + "]";
	}
	
	
}
