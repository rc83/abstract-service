package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.AbstractInformationEntity;
import com.example.entity.ApplicationInformationEntity;
import com.example.exception.ResourceNotFoundException;
import com.example.model.UpdateAbstractData;
import com.example.repository.AbstractRepository;
import com.example.repository.AppRepository;

@Service
public class AbstractService {
	
	@Autowired
	private AppRepository appRepository;
	
	@Autowired
	private AbstractRepository absRepository;
	
	/**
	 * Service to return all applications
	 * @return
	 */
	public Iterable<AbstractInformationEntity> getAllAbstracts(){		
		return absRepository.findAll();
	}
	
	/**
	 * Service to delete an abstract.
	 * @return
	 */
	public String deleteAbstract(int abstractId){		
		absRepository.deleteById(abstractId);
		return "Abstract record deleted.";
	}
	
	
	/**
	 * Service to get a single abstract.
	 * @return
	 */
	public AbstractInformationEntity getSingleAbstract(int abstractId){		
		return absRepository.findById(abstractId).orElseThrow(() ->new ResourceNotFoundException(abstractId));
	}


	/**
	 * Update the selected abstract entity.
	 */
	public String updateAbstractData(int id, UpdateAbstractData abstractData) {
		// Find the abstract data for the id
		AbstractInformationEntity updatedAbstract = absRepository.findById(id).orElseThrow(() ->new ResourceNotFoundException(id));
		
		// Find the app data to update the indicator
		Integer appId = abstractData.getApplicationId();
		ApplicationInformationEntity appInfoEntity = appRepository.findById(appId).orElseThrow(() -> new ResourceNotFoundException(appId));
		
		appInfoEntity.setAbstractCompleteInd(abstractData.getAbstractCompleteInd());
		
		updatedAbstract.setComments(abstractData.getComments());
		updatedAbstract.setDateSelected(abstractData.getDateSelected());
		updatedAbstract.setLastModifiedOn(abstractData.getLastModifiedOn());
		updatedAbstract.setModifiedUser(abstractData.getModifiedUser());
		updatedAbstract.setAbstractFileName(abstractData.getAbstractFileName());
		
		absRepository.save(updatedAbstract);
		appRepository.save(appInfoEntity);
		
		return "Abstract Data saved.";
	}

	/**
	 * Create a fresh abstract.
	 * @param abstractData
	 * @return
	 */
	public AbstractInformationEntity createAbstract(AbstractInformationEntity abstractData) {
		// TODO Auto-generated method stub
		return absRepository.save(abstractData);
	}
}
